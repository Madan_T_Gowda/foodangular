import { Dish } from './dish';


export const DISHES: Dish[] =  [
    { 
      name: 'Utappizza',
      image: '/assets/images/food1.jpg',
      category: 'mains',
      label: 'hot',
      price: '55.9',
      description: 'A unique combination of indian uttappam',
        comments: [
            {
                rating:5 ,
                author:"Madan T",
                comment:"Sooper Utappizza",
                mdate:"2018-03-20"
            },

            {
                rating:5,
                author:"Gandhrava",
                comment:"good Utappizza",
                mdate:"2018-04-22"

            },
            {
                rating:1,
                author:"Sharath",
                comment:"I dont like it Utappizza",
                mdate:"2018-05-20"

            },

        ]
    },
    
      { 
        name: 'Fishsal',
        image: '/assets/images/food2.jpg',
        category: 'mains',
        label: 'hot',
        price: '60.9',
        description: 'A unique combination of kundapura fish and indian fishsal',
        comments: [
            {
                rating:5,
                author:"Madan T",
                comment:"Sooper Fishsal",
                mdate:"2018-03-20"
            },

            {
                rating:5,
                author:"Gandhrava",
                comment:"good Fishsal",
                mdate:"2018-04-22"

            },
            {
                rating:1,
                author:"Sharath",
                comment:"I dont like this Fishsal",
                mdate:"2018-05-20"

            },

        ]
        
    
    },
      
       { 
          name: 'Cornsticks',
          image: '/assets/images/food3.jpg',
          category: 'appetizer',
          label: 'hot',
          price: '50.9',
          description: 'A unique combination of salt and indian cornsticks',
          comments: [
            {
                rating:5,
                author:"Madan T",
                comment:"Sooper Cornsticks",
                mdate:"2018-03-20"
            },

            {
                rating:5,
                author:"Gandhrava",
                comment:"good Cornsticks",
                mdate:"2018-04-22"

            },
            {
                rating:1,
                author:"Sharath",
                comment:"I dont like this Cornsticks",
                mdate:"2018-05-20"

            },

        ]
        
        
        },
        
          { 
            name: 'burger',
            image: '/assets/images/food4.jpg',
            category: 'appetizer',
            label: 'hot',
            price: '25.9',
            description: 'A unique combination of indian veggies and indian burger',
            comments: [
                {
                    rating:5,
                    author:"Madan T",
                    comment:"Sooper burger",
                    mdate:"2018-03-20"
                },
    
                {
                    rating:5,
                    author:"Gandhrava",
                    comment:"good burger",
                    mdate:"2018-04-22"
    
                },
                {
                    rating:1,
                    author:"Sharath",
                    comment:"I dont like this burger",
                    mdate:"2018-05-20"
    
                },
    
            ]
        
        },
        
            { 
              name: 'saldesert',
              image: '/assets/images/food5.jpg',
              category: 'desert',
              label: '',
              price: '20.9',
              description: 'A unique combination of indian salads',
              comments: [
                {
                    rating:5,
                    author:"Madan T",
                    comment:"Sooper saldesert",
                    mdate:"2018-03-20"
                },
    
                {
                    rating:5,
                    author:"Gandhrava",
                    comment:"good saldesert",
                    mdate:"2018-04-22"
    
                },
                {
                    rating:2,
                    author:"Sharath",
                    comment:"I dont like this saldesert",
                    mdate:"2018-05-20"
    
                },
    
            ]
            
            },
  
              { 
                name: 'CrispyChicken',
                image: '/assets/images/food6.jpg',
                category: 'mains',
                label: 'hot',
                price: '155.9',
                description: 'Deep fried indian nati chicken',
                comments: [
                    {
                        rating:5,
                        author:"Madan T",
                        comment:"Sooper CrispyChicken",
                        mdate:"2018-03-20"
                    },
        
                    {
                        rating:5,
                        author:"Gandhrava",
                        comment:"good CrispyChicken",
                        mdate:"2018-04-22"
        
                    },
                    {
                        rating:3,
                        author:"Sharath",
                        comment:"I dont like this CrispyChicken",
                        mdate:"2018-05-20"
        
                    },
        
                ]
            
            }
    
  ];