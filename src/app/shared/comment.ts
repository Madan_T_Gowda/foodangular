export class Comment{
    rating: number;
    comment: string;
    author: string;
    mdate: string;
}